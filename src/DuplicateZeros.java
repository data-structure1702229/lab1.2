public class DuplicateZeros {
    public static void main(String[] args) throws Exception {
        int arr[] = {1,0,2,3,0,4,5,0};

        System.out.print("arr = [");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(+arr[i]);
            if(i==arr.length-1){
                break;
            }
            System.out.print(",");
        }
        System.out.println("] ");


        for (int i = arr.length - 1; i >= 0; i--) {
            if (arr[i] == 0) {
                for (int j = arr.length - 1; j > i; j--) {
                    arr[j] = arr[j - 1];
                }
            }
        }

        System.out.print("[");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(+arr[i]);
             if(i==arr.length-1){
                break;
            }
            System.out.print(",");
        }
        System.out.print("] ");
    }
}
